Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '10.3'
s.name = "MonetSdk"
s.summary = "MonetSdk lets a user select an ice cream flavor."
s.requires_arc = true

# 2
s.version = "0.1.3"

# 3
#s.license = { :type => "MIT", :file => "LICENSE" }
#s.license = { :type => "MIT", :file => "License.txt" }
#s.license = { :type => "MIT", :file => "FILE_LICENSE" }

  s.license      = "MIT (alkfjaslfjlsjfsjfjdfdlfjlsjfsjfjdflfjlsjfsjfjdfsjfdsjfslfkdsjfjflkjdsfjdjfljflkjflk
)"




# 4 - Replace with your name and e-mail address
s.author = { "Akash Bhardwaj" => "Akash.bhardwaj@ashmar.in" }

# 5 - Replace this URL with your own GitHub page's URL (from the address bar)
s.homepage = "https://bitbucket.org/akash_ashmar/monetsdk/src"

# 6 - Replace this URL with your own Git URL from "Quick Setup"
s.source = { :git => "https://akash_ashmar@bitbucket.org/akash_ashmar/monetsdk.git", 
             :tag => "#{s.version}" }

# 7
s.framework = "UIKit"
s.dependency 'HaishinKit', '~> 0.10.5'


# 8
s.source_files = "MonetSdk/**/*.{swift}"

# 9
s.resources = "MonetSdk/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"

# 10
s.swift_version = "4.2"

end
