Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '10.3'
s.name = "MonetSdk"
s.summary = "MonetSdk lets the app detect and analyse users' emossions and reactions while watching the app's content on iOS devices. MonetSdk empowers the app to conduct surveys, which can help to improve product."

s.requires_arc = true

# 2
s.version = "0.1.10"

# 3
#s.license = { :type => "MIT", :file => "LICENSE" }
#s.license = { :type => "MIT", :file => "License.txt" }
#s.license = { :type => "MIT", :file => "FILE_LICENSE" }

  s.license      = "MIT (alkfjaslfjlsjfsjfjdfdlfjlsjfsjfjdflfjlsjfsjfjdfsjfdsjfslfkdsjfjflkjdsfjdjfljflkjflk
)"




# 4 - Replace with your name and e-mail address
s.author = { "Akash Bhardwaj" => "Akash.bhardwaj@ashmar.in" }

# 5 - Replace this URL with your own GitHub page's URL (from the address bar)
s.homepage = "https://bitbucket.org/akash_ashmar/monetsdk/src"

# 6 - Replace this URL with your own Git URL from "Quick Setup"
 s.source = { :git => "https://akash_ashmar@bitbucket.org/akash_ashmar/monetsdk.git", 
             :tag => "#{s.version}" }
#s.source = { :path => '.' }


# 7
s.framework = "UIKit"
#s.dependency 'HaishinKit', '~> 0.10.6'


# 8
s.source_files = "MonetSdk/**/*.{swift}"

# 9
s.resources = "MonetSdk/**/*.{pdf,png,jpeg,jpg,storyboard,xib,xcassets}"
s.resources = "Resources/**/*.storyboard"

# 10
s.swift_version = "4.2"

end
